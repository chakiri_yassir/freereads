<?php

namespace App\Controller\Admin;

use App\Controller\Admin\Trait\ReadCreateDeleteTrait;
use App\Entity\Invitation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class InvitationCrudController extends AbstractCrudController
{
    use ReadCreateDeleteTrait;

    public static function getEntityFqcn(): string
    {
        return Invitation::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email'),
            AssociationField::new('reader')->hideWhenCreating(),
            TextField::new('uuid')->hideWhenCreating(),
        ];
    }
}
