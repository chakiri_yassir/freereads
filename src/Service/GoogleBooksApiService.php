<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GoogleBooksApiService
{
    private const URL = 'volumes/';

    public function __construct(
        private readonly HttpClientInterface $googlebooksClient
    ) {
    }

    /**
     * @return array|mixed[]
     */
    public function get(string $id): array
    {
        return $this->makeRequest('GET', self::URL.$id);
    }

    /**
     * @return array|mixed[]
     */
    public function search(string $search): array
    {
        if (strlen($search) < 3) {
            return [];
        }

        return $this->makeRequest('GET', self::URL, [
            'query' => [
                'q' => $search,
            ],
        ]);
    }

    /**
     * @param array|mixed[] $options
     *
     * @return array|mixed[]
     */
    private function makeRequest(string $method, string $url, ?array $options = []): array
    {
        $response = $this->googlebooksClient->request($method, $url, $options);

        return $response->toArray();
    }
}
